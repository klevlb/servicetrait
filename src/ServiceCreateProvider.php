<?php

use Illuminate\Support\ServiceProvider;

class ServiceCreateProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->commands([
            MakeServiceCommand::class
        ]);

        $this->publishes([
            __DIR__.'/ServiceTrait.php' => base_path('app/Traits/ServiceTrait.php'),
            __DIR__ . '/service.stub' => base_path('stubs/service.stub')
        ]);
    }
}
